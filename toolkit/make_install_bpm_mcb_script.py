
from generic_parser import EntryPointParameters, entrypoint
from pathlib import Path
import re
import tfs

BPM_LENGTH = 0.1 # length of the BPM in the sequence
MCB_LENGTH = 0.2 # length of the orbitcorrector in the sequence

# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="tfs_file",
        type=str,
        required=True,
        help="Path to the tfs aperture file.",
    )
    params.add_parameter(
        name="focussing_quad",
        type=str,
        required=True,
        help="Regex pattern to identfy a focussing arc quadrupole.",
    )
    params.add_parameter(
        name="defocussing_quad",
        type=str,
        required=True,
        help="Regex pattern to identfy a defocussing arc quadrupole.",
    )
    params.add_parameter(
        name="output_file",
        type=str,
        default='install_bpm_orbitcorrectors.madx',
        help="Path to outputfile.",
    )
    return params

@entrypoint(get_params(), strict=True)
def main(opt):

    twiss_df = tfs.read(opt.tfs_file, index='NAME')

    # select focussing and defocussing arc quadrupoles from twiss_df
    twiss_df = twiss_df.loc[twiss_df.index.str.contains(f'{opt.focussing_quad}|{opt.defocussing_quad}')]

    bpm_definition= ''
    mcb_definition= ''
    install_string = 'SEQEDIT, SEQUENCE=FCCEE_P_RING;\n'

    for name, data in twiss_df.iterrows():
        kick_plane = 'H' if bool(re.search(opt.focussing_quad, name)) else 'V'
        bpm_definition+=f'BPM.{name}: MONITOR, L={BPM_LENGTH};\n'
        mcb_definition+=f'MCB.{name}: {kick_plane}KICKER, {kick_plane}KICK:=kick.{name}, L={MCB_LENGTH};\n'
        # provide coordinate of center as given sequence definition
        install_string+=f'INSTALL, ELEMENT=BPM.{name}, AT={ data["S"] + 0.5*BPM_LENGTH };\n'
        install_string+=f'INSTALL, ELEMENT=MCB.{name}, AT={ data["S"] - data["L"] - 0.5*MCB_LENGTH };\n'
    install_string+= 'ENDEDIT;\n'

    with open(opt.output_file, 'w') as f:
        f.write(bpm_definition)
        f.write('\n')
        f.write(mcb_definition)
        f.write('\n')
        f.write(install_string)

# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()
